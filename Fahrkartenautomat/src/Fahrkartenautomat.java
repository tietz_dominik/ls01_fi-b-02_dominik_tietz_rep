﻿import java.util.Scanner;

class Fahrkartenautomat
{
    public static void main(String[] args)
    {
    	Scanner tastatur = new Scanner(System.in);
        
        double ticketPreis; //Preis eines Tickets wird gespeichert
        int ticketAnzahl; //Die Anzahl der Tickets wird gespeichert
        double zuZahlenderBetrag; //Der Gesammtpreis wird gespeichert
        double eingezahlterGesamtbetrag; //Der gesammte eingezahle Betrag wird gespeichert
        double eingeworfeneMünze; //Der als letztes eingezahlte Betrag wird gespeichert
        double rückgabebetrag; //Der Rückgabe betrag wird gespeichert
        

        /*
         * Bei allen variablen bis auf ticketAnzahl handelt es sich 
         * um double da der Preis nachkommastellen haben kann und man
         * diese so effektiv speichern kann.
         * 
         * Bei der Variable ticketAnzahl habe ich mich für für ein 
         * integer entschieden da die Anzahl nur ein ganzes sein kann,
         * denn es gibt keine halben Tickets.
         * 
         * Um den Gesammtpreis zu berechnen werden die Variablen 
         * ticketPreis und ticketAnzahl miteinander multipliziert
         * und in der Variable zuZahlenderBetrag gespeichert.
         * 
         * Was dabei zu beachten ist, dass ticketAnzahl ein integer ist
         * und ticketPreis ein double ist. Die können aber trotzdem
         * ohne Probleme multipliziert werden da java automatisch den
         * integer in ein double umwandelt.
         */
        while (true) {
	       // Geldeinwurf
	       // -----------
	        
	       zuZahlenderBetrag = fahrkartenbestellungErfassen(tastatur);
	       
	       rückgabebetrag = fahrkartenBezahlen(zuZahlenderBetrag, tastatur);
	
	       // Fahrscheinausgabe
	       // -----------------
	       fahrkartenAusgeben();
	
	       // Rückgeld Ausgabe
	       // -------------------------------
	       rueckgeldAusgeben(rückgabebetrag);
	       
	       System.out.println("\n\n\n\n\n\n\n\n");
        }
    }
    
    static double fahrkartenbestellungErfassen(Scanner tastatur) {
    	int ticketArt = 0;
    	double ticketPreis = 0;
    	double zuZahlenderBetrag = 0;
    	
    	while (ticketArt != 4) {
    	ticketArt = 0;
    	System.out.print("Wählen Sie ihre Wunschfahrkarte für Berlin AB aus:\n");
    	System.out.print("   Einzelfahrscheine (1)\n");
    	System.out.print("   Tageskarten (2)\n");
    	System.out.print("   Kleingruppen-Tageskarten (3)\n");
    	System.out.print("   Bezahlen (4)\n");
    	while ((ticketArt < 1 || ticketArt > 4)) {
    		ticketArt = tastatur.nextInt();
    		
    		if (ticketArt < 1 || ticketArt > 3) {
    			if (ticketArt != 4) {
    				System.out.print("Falsche Art Eingabe. Versuche es Erneut! \n");
    			} else if (zuZahlenderBetrag == 0) {
    				System.out.print("Sie müssen mindestens ein Ticket kaufen \n");
    			}
        		
        	}
    	}
        
    	if (ticketArt == 1) {
    		ticketPreis = 2.9;
    	} else if (ticketArt == 2) {
    		ticketPreis = 8.6;
    	} else if (ticketArt == 3) {
    		ticketPreis = 23.5;
    	}
    	
        int ticketAnzahl = 0;
        
        while ((ticketAnzahl < 1 || ticketAnzahl > 10) && ticketArt != 4) {
        	System.out.print("Anzahl der Tickets (Mindestens 1 und Höchstens 10) : ");
        	ticketAnzahl = tastatur.nextInt();
        	
        	if (ticketAnzahl < 1 || ticketAnzahl > 10) {
        		System.out.print("Falsche Anzahl. Versuche es Erneut! \n");
        	}
        	
        }
        zuZahlenderBetrag += ticketPreis * ticketAnzahl;
        ticketPreis = 0;
    	}
        
        return zuZahlenderBetrag;
    }
    
    static double fahrkartenBezahlen (double zuZahlenderBetrag, Scanner tastatur) {
    	double eingezahlterGesamtbetrag = 0.00;
        while(eingezahlterGesamtbetrag < zuZahlenderBetrag)
        {
     	   System.out.printf( "%s%.2f%s\n", "Noch zu zahlen: ",(zuZahlenderBetrag - eingezahlterGesamtbetrag)," Euro");
     	   System.out.print("Eingabe (mind. 5Ct, höchstens 2 Euro): ");
     	   double eingeworfeneMünze = tastatur.nextDouble();
            eingezahlterGesamtbetrag += eingeworfeneMünze;
        }
        return (int)((eingezahlterGesamtbetrag - zuZahlenderBetrag) * 100);
    }
    
    static void fahrkartenAusgeben() {
    	System.out.println("\nFahrschein wird ausgegeben");
        for (int i = 0; i < 8; i++)
        {
           System.out.print("=");
           try {
 			Thread.sleep(250);
 		} catch (InterruptedException e) {
 			// TODO Auto-generated catch block
 			e.printStackTrace();
 		}
        }
        System.out.println("\n\n");
    }
    
    static void rueckgeldAusgeben(double rückgabebetrag) {
    	if(rückgabebetrag > 0)
        {
     	   System.out.printf( "%s%.2f%s\n", "Der Rückgabebetrag in Höhe von ", (((double)rückgabebetrag) / 100.0), " EURO");
     	   System.out.println("wird in folgenden Münzen ausgezahlt:");
            while(rückgabebetrag >= 200) // 2 EURO-Münzen
            {
         	  System.out.println("2 EURO");
 	          rückgabebetrag -= 200;
            }
            while(rückgabebetrag >= 100) // 1 EURO-Münzen
            {
         	  System.out.println("1 EURO");
 	          rückgabebetrag -= 100;
            }
            while(rückgabebetrag >= 50) // 50 CENT-Münzen
            {
         	  System.out.println("50 CENT");
 	          rückgabebetrag -= 50;
            }
            while(rückgabebetrag >= 20) // 20 CENT-Münzen
            {
         	  System.out.println("20 CENT");
  	          rückgabebetrag -= 20;
            }
            while(rückgabebetrag >= 10) // 10 CENT-Münzen
            {
         	  System.out.println("10 CENT");
 	          rückgabebetrag -= 10;
            }
            while(rückgabebetrag >= 5)// 5 CENT-Münzen
            {
         	  System.out.println("5 CENT");
  	          rückgabebetrag -= 5;
            }
        }

        System.out.println("\nVergessen Sie nicht, den Fahrschein\n"+
                           "vor Fahrtantritt entwerten zu lassen!\n"+
                           "Wir wünschen Ihnen eine gute Fahrt.");
    }
}