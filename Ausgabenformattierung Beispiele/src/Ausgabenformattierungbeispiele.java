
public class Ausgabenformattierungbeispiele {

	public static void main(String[] args) {
		String s = "Java-Programm";

		System.out.printf( "\n|%s|\n", s );
		System.out.printf( "|%20s|\n", s );
		System.out.printf( "|%-20s|\n", s );
		System.out.printf( "|%5s|\n", s );
		System.out.printf( "|%.4s|\n", s );
		System.out.printf( "|%20.4s|\n", s );


	}

}